open CDG
open Depgram
open Extensions

type primitive = Prim of Type.primitive | Star of depname
[@@deriving eq, show]

let depname dep = Prim (Type.Depname dep)
let anchor dir dep = Prim (Type.Anchor (dir, dep))
let root = Prim Type.Root
let prim prim = Prim prim
let star dep = Star dep

let primitive_to_string prim = match prim with
  | Prim p -> Type.primitive_to_string p
  | Star dep -> UD.Deprel.to_string dep ^ "*"

let get_depname t =
  match t with
  | Star d -> d
  | Prim (Depname d) -> d
  | _ -> failwith ":( no depname found"

type t = {
  left : primitive list;
  head : Type.primitive;
  right : primitive list;
  potential : Type.potential;
} [@@deriving eq, show]

let head prim = { left = []; head = prim; right = []; potential = [] }

let (!) dep  = head (Depname dep)
let lanc dep = head (Type.Anchor (Valency.Left, dep))
let ranc dep = head (Type.Anchor (Valency.Right, dep))

let (>>) l r = { r with left = l.left @ [Prim l.head] @ l.right @ r.left }
let (<<) l r = { l with right = l.right @ r.left @ [Prim r.head] @ r.right }
let (^^) c p = { c with potential = c.potential @ p }

let ( *>> ) l r = match l.head with
  | Depname dep -> { r with left = l.left @ [Star dep] @ l.right @ r.left }
  | _ -> failwith "Star only works with a depname"

let ( <<* ) l r = match r.head with
  | Depname dep -> { l with right = l.right @ r.left @ [Star dep] @ r.right }
  | _ -> failwith "Star only works with a depname"

let inner_left t  = List.last t.left
let outer_left t  = List.first t.left
let inner_right t = List.first t.right
let outer_right t = List.last t.right

let drop_inner_left  t = {t with left  = List.drop_last t.left}
let drop_outer_left  t = {t with left  = try List.tl t.left with _ -> []}
let drop_inner_right t = {t with right = try List.tl t.right with _ -> []}
let drop_outer_right t = {t with right = List.drop_last t.right}

let feed_left prim t =
  let (let*) = Option.bind in
  let* prim_left = inner_left t in

  match prim, prim_left with
  | Type.Depname dep, Star dep' ->
    if UD.Deprel.equal dep dep'
    then Some t
    else None
  | prim, Prim prim' ->
    if Type.equal_primitive prim prim'
    then Some (drop_inner_left t)
    else None
  | _, _ -> None

let feed_right t prim =
  let (let*) = Option.bind in
  let* prim_right = inner_right t in

  match prim, prim_right with
  | Type.Depname dep, Star dep' ->
    if UD.Deprel.equal dep dep'
    then Some t
    else None
  | prim, Prim prim' ->
    if Type.equal_primitive prim prim'
    then Some (drop_inner_right t)
    else None
  | _, _ -> None

let rec subsumes_right r1 r2 =
  match r1, r2 with
  | [], [] -> true
  | [], _  -> false

  | (Star _) :: t, [] -> subsumes_right t []
  | _, [] -> false

  | (Star d) :: t, (Prim (Type.Depname d')) :: t' ->
    if equal_depname d d'
    then subsumes_right ((Star d) :: t) t'
    else subsumes_right t ((depname d') :: t)

  | h :: t, h' :: t' -> equal_primitive h h' && subsumes_right t t'

let subsumes_left l1 l2 = subsumes_right (List.rev l1) (List.rev l2)

let subsumes t1 t2 =
  Type.equal_primitive t1.head t2.head
  && Type.equal_potential t1.potential t2.potential
  && subsumes_right t1.right t2.right
  && subsumes_left t1.left t2.left

let to_string t =
    "["
    ^ (List.map primitive_to_string t.left |> String.concat "\\")
    ^ (if List.is_empty t.left then "" else "\\")
    ^ Type.primitive_to_string t.head
    ^ (if List.is_empty t.right then "" else "/")
    ^ (List.map primitive_to_string t.right |> String.concat "/")
    ^ "]"
    ^ Type.potential_to_string t.potential

let remove_duplicates prims =
  let rec remove_duplicates' last prims =
    match prims with
    | [] -> []
    | (Star s) :: t ->
      if equal_primitive (Star s) last
      then remove_duplicates' (Star s) t
      else (Star s) :: remove_duplicates' (Star s) t
    | h :: t -> h :: remove_duplicates' h t
  in
  match prims with
  | [] -> []
  | h :: t -> h :: remove_duplicates' h t

module Simple = struct

  let rec contiguous d prims =
    match prims with
    | [] -> []
    | Prim (Depname d') :: t ->
      if equal_depname d d'
      then depname d' :: contiguous d t
      else []
    | (Star s) :: t -> Star s :: contiguous d t
    | _ -> []

  let should_generalise k (prims : primitive list) =
    match prims with
    | []                       -> false
    | Prim Type.Root :: _       -> false
    | Prim (Type.Anchor _) :: _ -> false

    | h :: _ ->
      let d = get_depname h in
      let l = contiguous d prims in
      let depnames = List.find_all (equal_primitive (depname d)) l in
      let generalised = List.find_all (equal_primitive (star d)) l in

      List.length depnames >= k
      || (generalised <> [] && depnames <> [])

  let rec generalise_where k prims =
    match prims with
    | [] -> ([], [])
    | h :: t -> if should_generalise k prims
        then ([], prims)
        else
          let (start, to_gen) = generalise_where k t in
          (h :: start, to_gen)

  let add_stars prims =
    let rec add_stars' d prims =
      match prims with
      | [] -> []
      | (Prim (Depname d')) :: t ->
        if equal_depname d d'
        then star d' :: add_stars' d t
        else prims
      | h :: t -> h :: add_stars' d t
    in
    match prims with
    | [] -> []
    | h :: _ -> add_stars' (get_depname h) prims

  let rec generalise_side k prims =
    match generalise_where k prims with
    | init, [] -> init
    | init, to_gen ->
      init @ (to_gen |> add_stars |> remove_duplicates)
      |> generalise_side k

  let generalise ?(k=2) (t : Type.t) =
    let left = t.left |> List.map prim in
    let right = t.right |> List.map prim in
    {
      left = generalise_side k left;
      head = t.head;
      right = generalise_side k right;
      potential = t.potential;
    }
end

module Global = struct
  let is_depname t = match t with
    | Prim (Type.Depname _) -> true
    | _ -> false

  let non_global_k_star k prims d =
    let ndepnames = prims |> List.filter (equal_primitive (depname d))
                  |> List.length
    in
    ndepnames >= k
    || (ndepnames >= 1 && prims |> List.exists (equal_primitive (star d)))

  let add_stars ds =
    let depnames = List.map depname ds in
    List.map (fun x ->
        if not (List.exists (equal_primitive x) depnames)
        then x
        else star (get_depname x)
      )

  let generalise_side k prims =
    let to_generalise = prims
                        |> List.filter is_depname
                        |> List.map get_depname
                        |> List.filter (non_global_k_star k prims)
    in
    add_stars to_generalise prims |> remove_duplicates

  let generalise ?(k=2) (t : Type.t) =
    let left = t.left |> List.map prim in
    let right = t.right |> List.map prim in
    {
      left = generalise_side k left;
      head = t.head;
      right = generalise_side k right;
      potential = t.potential;
    }
end
