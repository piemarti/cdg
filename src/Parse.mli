(** CDG parser *)

type type_choice = KStar.t list
(** list of possible types for a word *)

type untyped_sentence = type_choice list
(** a sentence represented as a list of type choices *)

type typed_sentence = KStar.t list
(** a typed sentence: a list of KStar CDG types *)

type item = {
  t : KStar.t;              (** Item type *)
  deficits_l : int Array.t; (** Left deficits *)
  deficits_r : int Array.t; (** Right deficits *)
  item_l : item option;     (** Parent items on the left *)
  item_r : item option;     (** Parent items on the right *)
}
(** Items used by the parsing algorithm *)

val parse : untyped_sentence -> item list
(** Parses an untyped sentence into a list of items *)

val item_to_typed_sentence : item -> typed_sentence
(** Extracts the typed sentence from the item *)
