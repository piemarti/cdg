open CDG
open Depgram
open Extensions

type type_choice = KStar.t list
type untyped_sentence = type_choice list
type typed_sentence = KStar.t list

let is_left_bracket (v : Valency.t) : bool =
  Valency.is_left_neg v || Valency.is_right_pos v

let is_right_bracket (v : Valency.t) : bool =
  Valency.is_left_pos v || Valency.is_right_neg v

let compare_valencies (v1 : Valency.t) (v2 : Valency.t) =
  let open Valency in
  match v1.direction, v1.polarity, v2.direction, v2.polarity with
  | Left, _, Right, _ -> -1
  | Right, _, Left, _ ->  1
  | _, Negative, _, Positive -> -1
  | _, Positive, _, Negative ->  1
  | _, _, _, _ -> String.compare
                    (UD.Deprel.to_string v1.depname)
                    (UD.Deprel.to_string v2.depname)

type item = {
  t : KStar.t;
  deficits_l : int Array.t;
  deficits_r : int Array.t;
  item_l : item option;
  item_r : item option;
}

let item t deficits_l deficits_r item_l item_r =
  {t; deficits_l; deficits_r; item_l; item_r}

let equal_item i1 i2 =
  KStar.equal i1.t i2.t
  && (Array.for_all2 (=) i1.deficits_l i2.deficits_l)
  && (Array.for_all2 (=) i1.deficits_r i2.deficits_r)

type state = {
  sentence : type_choice Array.t;
  left_brackets : Valency.t Array.t;
  fail_left : int Array.t Array.t;
  fail_right : int Array.t Array.t;
  mutable matrix : (item list) Array.t Array.t;
}

let words state = Seq.range 0 (Array.length state.sentence - 1)
let word state i = state.sentence.(i)
let brackets state =
  match Array.length state.left_brackets with
  | 0 -> Seq.empty
  | n -> Seq.range 0 (n - 1)
let lbracket state i = state.left_brackets.(i)
let rbracket state i = Valency.dual state.left_brackets.(i)
let lfail state v i = state.fail_left.(v).(i)
let rfail state v i = state.fail_right.(v).(i)

let m_get    state i j   = state.matrix.(i).(j)
let m_update state i j f = state.matrix.(i).(j) <- (f state.matrix.(i).(j))

let left_brackets (sentence : untyped_sentence) : Valency.t Array.t option =
    let all_brackets = sentence
      |> List.map (List.map KStar.(fun t -> t.potential))
      |> List.concat |> List.concat
    in
    let left_brackets = all_brackets
                        |> List.filter is_left_bracket
                        |> List.sort_uniq compare_valencies
    in
    let right_brackets = all_brackets
                         |> List.filter is_right_bracket
                         |> List.sort_uniq compare_valencies
    in

    let is_dual l r = Valency.equal l (Valency.dual r) in
    let is_ok =
      List.length left_brackets = List.length right_brackets
      && List.for_all2 is_dual left_brackets right_brackets in
    if is_ok
    then Some (Array.of_list left_brackets)
    else None

let deficit (v : Valency.t) (pot : Type.potential) : int =
  ( if is_left_bracket v
    then List.rev pot
    else pot )
  |> List.scanl (fun prev v' ->
      if Valency.equal v v'                     then prev + 1
      else if Valency.equal v (Valency.dual v') then prev - 1
                                                else prev
    ) 0
  |> List.max Int.compare 0

let pi_inc (v : Valency.t) (pi_prev : int) (types : type_choice) : int =
  let open KStar in
  types
  |> List.map (fun t ->
      deficit v t.potential
      + max 0 (pi_prev - deficit (Valency.dual v) t.potential)
    )
  |> List.max Int.compare 0

let fail_left (sentence : untyped_sentence) (left_brackets : Valency.t Array.t) =
  left_brackets |> Array.map (fun v ->
      List.to_seq sentence
      |> Seq.scanl (pi_inc v) 0
      |> Array.of_seq
    )

let fail_right (sentence : untyped_sentence) (left_brackets : Valency.t Array.t) =
  left_brackets |> Array.map (fun v ->
      List.rev sentence |> List.to_seq
      |> Seq.scanl (pi_inc (Valency.dual v)) 0
      |> Array.of_seq
    )

let rec init_state (sentence : untyped_sentence) : state =
  let n = List.length sentence in
  match left_brackets sentence with
  | Some left_brackets -> { sentence = Array.of_list sentence;
                           left_brackets;
                           fail_left = fail_left sentence left_brackets;
                           fail_right = fail_right sentence left_brackets;
                           matrix = Array.make_matrix n n [];
                          }
  | None -> init_state (sentence
                        |> List.map
                          (List.map (fun t -> KStar.{t with potential = []})))

let rec add_item (state : state) (item : item) (items : item list) : item list =
  let items = List.union equal_item items [item] in
  let items = match KStar.outer_left item.t with
    | Some (KStar.Star _) ->
      add_item state {item with t = KStar.drop_outer_left item.t} items
    | _ -> items
  in
  match KStar.outer_right item.t with
  | Some (KStar.Star _) ->
    add_item state {item with t = KStar.drop_outer_right item.t} items
  | _ -> items

let propose (state : state) (i : int) : unit =
  let open KStar in
  let n = Array.length state.sentence in
  word state i |> List.iter (fun t ->
      let deficits_l = brackets state |> Seq.map (fun v ->
          deficit (lbracket state v) t.potential
        ) |> Array.of_seq
      in

      let deficits_r = brackets state |> Seq.map (fun v ->
          deficit (rbracket state v) t.potential
        ) |> Array.of_seq
      in

      let goodC = brackets state |> Seq.for_all (fun v ->
          deficits_l.(v) <= rfail state v (n - 1 - i)
          && deficits_r.(v) <= lfail state v i
        )
      in

      if goodC then
        m_update state i i
          (add_item state (item t deficits_l deficits_r None None))
    )

let combine_left (l : KStar.t) (r : KStar.t) : KStar.t option =
  let (let*) = Option.bind in

  let* prim = if List.is_empty l.left && List.is_empty l.right
    then Some l.head
    else None
  in

  KStar.feed_left prim r

let combine_right (l : KStar.t) (r : KStar.t) : KStar.t option =
  let (let*) = Option.bind in

  let* prim = if List.is_empty r.left && List.is_empty r.right
    then Some r.head
    else None
  in

  KStar.feed_right l prim

let subordinate_l (state : state) (i : int) (k : int) (j : int) : unit =
  let n = Array.length state.sentence in
  let i1s = m_get state i k in
  let i2s = m_get state (k + 1) j in
  let item_pairs = i1s |> List.concat_map (fun i1 ->
      i2s |> List.map (fun i2 -> (i1, i2))
    )
  in

  item_pairs |> List.iter (fun (i1, i2) ->
      let deficits_l = brackets state |> Seq.map (fun v ->
          i2.deficits_l.(v) + max 0 (i1.deficits_l.(v) - i2.deficits_r.(v))
        ) |> Array.of_seq
      in

      let deficits_r = brackets state |> Seq.map (fun v ->
          i1.deficits_r.(v) + max 0 (i2.deficits_r.(v) - i1.deficits_l.(v))
        ) |> Array.of_seq
      in

      let good_items = brackets state |> Seq.for_all (fun v ->
          deficits_l.(v) <= rfail state v (n - 1 - j)
          && deficits_r.(v) <= lfail state v i
        )
      in

      if good_items then
        match combine_left i1.t i2.t with
        | Some t -> m_update state i j
                      (add_item state
                         (item t deficits_l deficits_r (Some i1) (Some i2)))
        | _ -> ()
    )

let subordinate_r (state : state) (i : int) (k : int) (j : int) : unit =
  let n = Array.length state.sentence in
  let i1s = m_get state i k in
  let i2s = m_get state (k + 1) j in
  let item_pairs = i1s |> List.concat_map (fun i1 ->
      i2s |> List.map (fun i2 -> (i1, i2))
    )
  in

  item_pairs |> List.iter (fun (i1, i2) ->
      let deficits_l = brackets state |> Seq.map (fun v ->
          i2.deficits_l.(v) + max 0 (i1.deficits_l.(v) - i2.deficits_r.(v))
        ) |> Array.of_seq
      in

      let deficits_r = brackets state |> Seq.map (fun v ->
          i1.deficits_r.(v) + max 0 (i2.deficits_r.(v) - i1.deficits_l.(v))
        ) |> Array.of_seq
      in

      let goodItems = brackets state |> Seq.for_all (fun v ->
          deficits_l.(v) <= rfail state v (n - 1 - j)
          && deficits_r.(v) <= lfail state v i
        )
      in

      if goodItems then
        match combine_right i1.t i2.t with
        | Some t -> m_update state i j
                      (add_item state
                         (item t deficits_l deficits_r (Some i1) (Some i2)))
        | _ -> ()
    )

let check_item (item : item) : bool =
  KStar.equal item.t (KStar.head Type.Root)
  && Array.for_all (fun d -> d = 0) item.deficits_l
  && Array.for_all (fun d -> d = 0) item.deficits_r

let parse (sentence : untyped_sentence) : item list =
  let state = init_state sentence in
  let n = Array.length state.sentence in

  words state |> Seq.iter (propose state);

  for l = 1 to n - 1 do
    for i = 0 to n - 1 - l do
      let j = i + l in
      for k = i to j-1 do
        subordinate_l state i k j;
        subordinate_r state i k j;
      done;
    done
  done;

  (m_get state 0 (n-1)) |> List.find_all check_item

let rec item_to_typed_sentence (item : item) : typed_sentence =
  match item.item_l, item.item_r with
  | Some l, Some r -> (item_to_typed_sentence l) @ (item_to_typed_sentence r)
  | None, None -> [item.t]
  | _ -> failwith "item_to_typed_sentence: unbalanced tree (shouldn't happen)"
