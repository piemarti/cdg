(** Categorial dependency grammars *)

type depname = Depgram.UD.Deprel.t [@@deriving eq, show]

module Valency : sig
  (** Polarised valencies *)

  type direction = Left | Right
  [@@deriving eq, show]
  (** Valency direction*)

  val dual_direction : direction -> direction
  (** Returns the opposite direction *)

  val is_left : direction -> bool
  (** Returns true if the direction is Left*)

  val is_right : direction -> bool
  (** Returns true if the direction is Right*)

  type polarity = Positive | Negative
  [@@deriving eq, show]
  (** Valency polarity *)

  val dual_polarity : polarity -> polarity
  (** Returns the opposite polarity *)

  val is_positive : polarity -> bool
  (** Returns true if the polarity is Positive *)

  val is_negative : polarity -> bool
  (** Returns true if the polarity is Negative *)

  type t = {
    direction : direction; (** Direction of the valency *)
    polarity : polarity;   (** Polarity of the valency *)
    depname : depname;     (** Dependency name of the valency *)
  } [@@deriving eq, show]

  val make : direction -> polarity -> depname -> t
  (** Make a polarised valency *)

  val left_pos  : depname -> t
  (** Make a Left Positive valency *)

  val right_pos : depname -> t
  (** Make a Right Positive valency *)

  val left_neg  : depname -> t
  (** Make a Left Negative valency *)

  val right_neg : depname -> t
  (** Make a Right Negative valency *)

  val is_left_pos  : t -> bool
  (** Returns true if the valency is Left Positive *)

  val is_right_pos : t -> bool
  (** Returns true if the valency is Right Positive *)

  val is_left_neg  : t -> bool
  (** Returns true if the valency is Left Negative *)

  val is_right_neg : t -> bool
  (** Returns true if the valency is Right Negative *)

  val dual : t -> t
  (** Returns the dual valency (i.e. valency with dual polarity) *)

  val is_dual : t -> t -> bool
  (** Returns true if valencies are dual *)

  val to_string : t -> string
end

module Type : sig
  (** CDG type *)

  type primitive = Depname of depname                     (** Dependency name *)
                 | Anchor  of Valency.direction * depname (** Anchor *)
                 | Root                                   (** Root *)
  [@@deriving eq, show]
  (** Primitive type *)

  val depname : depname -> primitive
  (** Constructs a primitive type from a dependency name *)

  val anchor : Valency.direction -> depname -> primitive
  (** Constructs a primitive anchor type from a direction and dependency name *)

  val root : primitive
  (** The primitive type of the root *)

  val primitive_to_string : primitive -> string

  type potential = Valency.t list
  [@@deriving eq, show]
  (** Potential *)

  val potential_to_string : potential -> string

  type t = {
    left : primitive list;
    head : primitive;
    right : primitive list;
    potential : potential;
  }
  [@@deriving eq, show]
  (** A CDG type *)

  val head : primitive -> t
  (** Constructs a CDG type with the primitive as its head *)

  val (!) : depname -> t
  (** Constructs a CDG type with the dependency name as its head *)

  val lanc : depname -> t
  (** Constructs a CDG type with the left anchor as its head *)

  val ranc : depname -> t
  (** Constructs a CDG type with the right anchor as its head *)

  val (>>) : t -> t -> t
  (** [l >> r] appends the primitives of l to the left of r *)

  val (<<) : t -> t -> t
  (** [l << r] appends the primitives of r to the right of l *)

  val (^^) : t -> potential -> t
  (** [b ^^ p] appends p to the potential of b *)

  val inner_left : t -> primitive option
  val outer_left : t -> primitive option
  val inner_right : t -> primitive option
  val outer_right : t -> primitive option

  val drop_inner_left : t -> t
  val drop_outer_left : t -> t
  val drop_inner_right : t -> t
  val drop_outer_right : t -> t

  val feed_left : primitive -> t -> t option
  val feed_right : t -> primitive -> t option

  val return_type : ('word, depname) Depgram.Dependency.node -> primitive
  (** Head type of the word in a dependency structure *)

  val left_dependencies :
    ('word, depname) Depgram.Dependency.node -> primitive list
  (** Gathers the left dependencies of the word in a dependency structure *)

  val right_dependencies :
    ('word, depname) Depgram.Dependency.node -> primitive list
  (** Gathers the right dependencies of the word in a dependency structure *)

  val potential : ('word, depname) Depgram.Dependency.node -> potential
  (** Computes the potential of the word in a dependency structure *)

  val vicinity : ('word, depname) Depgram.Dependency.node -> t
  (** Vicinity of the word in a dependency structure *)

  val to_string : t -> string
end
