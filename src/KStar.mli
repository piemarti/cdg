(** KStar extension to CDG types *)

type primitive = Prim of CDG.Type.primitive (** CDG normal primitive type *)
               | Star of CDG.depname        (** Star type *)
[@@deriving eq, show]
(** New primitive type *)

val depname : CDG.depname -> primitive
(** Constructs a primitive type from a dependency name *)

val anchor : CDG.Valency.direction -> CDG.depname -> primitive
(** Constructs a primitive anchor type from a direction and dependency name *)

val root : primitive
(** The primitive type of the root *)

val prim : CDG.Type.primitive -> primitive
(** Constructs a KStar type from a normal CDG primitive type *)

val star : CDG.depname -> primitive
(** Constructs a KStar from a dependency name *)

val primitive_to_string : primitive -> string

type t = {
  left : primitive list;
  head : CDG.Type.primitive;
  right : primitive list;
  potential : CDG.Type.potential;
} [@@deriving eq, show]
(** A KStar CDG type *)

val head : CDG.Type.primitive -> t
(** Constructs a KStar CDG type with the primitive as its head *)

val (!) : Depgram.UD.Deprel.t -> t
(** Constructs a KStar CDG type with the dependency name as its head *)

val lanc : Depgram.UD.Deprel.t -> t
(** Constructs a KStar CDG type with the left anchor as its head *)

val ranc : Depgram.UD.Deprel.t -> t
(** Constructs a KStar CDG type with the right anchor as its head *)

val (>>) : t -> t -> t
(** [l >> r] appends the primitives of l to the left of r *)

val (<<) : t -> t -> t
(** [l << r] appends the primitives of r to the right of l *)

val (^^) : t -> CDG.Type.potential -> t
(** [b ^^ p] appends p to the potential of b *)

val ( *>> ) : t -> t -> t
(** [l *>> r] appends the primitives of l to the left of r with a star on l's
    head (assumes l's head is a Depname) *)

val ( <<* ) : t -> t -> t
(** [l <<* r] appends the primitives of r to the right of l with a star on r's
    head (assumes r's head is a Depname) *)

val inner_left : t -> primitive option
val outer_left : t -> primitive option
val inner_right : t -> primitive option
val outer_right : t -> primitive option

val drop_inner_left : t -> t
val drop_outer_left : t -> t
val drop_inner_right : t -> t
val drop_outer_right : t -> t

val feed_left : CDG.Type.primitive -> t -> t option
val feed_right : t -> CDG.Type.primitive -> t option

val subsumes : t -> t -> bool
(** [subsumes t1 t2] returns true if [t1] is more general than [t2] *)

val to_string : t -> string

(** Simple K-Star generalisation *)
module Simple : sig
  val generalise : ?k : int -> CDG.Type.t -> t
end

(** Simple Global K-Star generalisation *)
module Global : sig
  val generalise : ?k : int -> CDG.Type.t -> t
end
