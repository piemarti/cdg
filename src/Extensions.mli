(** Custom library extensions *)

module Dict : Map.S with type key = Text.t

(** Get rid of polymorphic comparison (greatly improves performance!): *)

val (=) : int -> int -> bool
val (<) : int -> int -> bool
val (>) : int -> int -> bool
val (<=) : int -> int -> bool
val (>=) : int -> int -> bool

module Array : sig
  include module type of Stdlib.Array
  val get_opt : 'a t -> int -> 'a option
  val scanl : ('a -> 'b -> 'a) -> 'a -> 'b t -> 'a t
  val to_seq_rev : 'a t -> 'a Seq.t
end

module List : sig
  include module type of Stdlib.List
  val drop_last : 'a t -> 'a t
  val enumerate : 'a t -> (int * 'a) Seq.t
  val first : 'a t -> 'a option
  val intersection : ('a -> 'a -> bool) -> 'a t -> 'a t -> 'a t
  val is_empty : 'a t -> bool
  val last : 'a t -> 'a option
  val max : ('a -> 'a -> int) -> 'a -> 'a t -> 'a
  val max_opt : ('a -> 'a -> int) -> 'a t -> 'a option
  val scanl : ('a -> 'b -> 'a) -> 'a -> 'b t -> 'a t
  val union : ('a -> 'a -> bool) -> 'a t -> 'a t -> 'a t
  val uniq : ('a -> 'a -> bool) -> 'a t -> 'a t
end

module Seq : sig
  include module type of Stdlib.Seq
  val exists : ('a -> bool) -> 'a t -> bool
  val find_first : ('a -> bool) -> 'a t -> 'a option
  val for_all : ('a -> bool) -> 'a t -> bool
  val range : ?step : int -> int -> int -> int Seq.t
  val scanl : ('a -> 'b -> 'a) -> 'a -> 'b t -> 'a t
  val skip : 'a t -> 'a t
end
