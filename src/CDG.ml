open Depgram
open Extensions

type depname = UD.Deprel.t [@@deriving eq, show]

module Valency = struct
  type direction = Left | Right
  [@@deriving eq, show]

  let dual_direction dir = match dir with
    | Left  -> Right
    | Right -> Left

  let is_left dir = equal_direction dir Left
  let is_right dir = equal_direction dir Right

  type polarity = Positive | Negative
  [@@deriving eq, show]

  let dual_polarity pol = match pol with
    | Positive -> Negative
    | Negative -> Positive

  let is_positive pol = equal_polarity pol Positive
  let is_negative pol = equal_polarity pol Negative

  type t = {
    direction : direction;
    polarity : polarity;
    depname : depname;
  } [@@deriving eq, show]

  let make direction polarity depname = { direction; polarity; depname }
  let left_pos  = make Left  Positive
  let right_pos = make Right Positive
  let left_neg  = make Left  Negative
  let right_neg = make Right Negative

  let is_left_pos  v = is_left v.direction  && is_positive v.polarity
  let is_right_pos v = is_right v.direction && is_positive v.polarity
  let is_left_neg  v = is_left v.direction  && is_negative v.polarity
  let is_right_neg v = is_right v.direction && is_negative v.polarity

  let dual v = { v with polarity = dual_polarity v.polarity }

  let is_dual v v' =
    UD.Deprel.equal v.depname v'.depname
    && equal_direction v.direction v'.direction
    && not (equal_polarity v.polarity v'.polarity)

  let to_string v = (match v.direction, v.polarity with
    | Left,  Positive -> "↖"
    | Left,  Negative -> "↙"
    | Right, Positive -> "↗"
    | Right, Negative -> "↘"
    )
    ^ UD.Deprel.to_string v.depname
end

module Type = struct
  type primitive = Depname of depname
                 | Anchor  of Valency.direction * depname
                 | Root
  [@@deriving eq, show]

  let depname dep = Depname dep
  let anchor dir dep = Anchor (dir, dep)
  let root = Root

  let primitive_to_string prim = match prim with
    | Depname dep -> UD.Deprel.to_string dep
    | Anchor (dir, dep) ->
      "#(" ^ Valency.(to_string (make dir Negative dep)) ^ ")"
    | Root -> "S"

  type potential = Valency.t list
  [@@deriving eq, show]

  let potential_to_string potential =
    if List.is_empty potential
    then ""
    else "<"
         ^ (List.map Valency.to_string potential |> String.concat ",")
         ^ ">"

  type t = {
    left : primitive list;
    head : primitive;
    right : primitive list;
    potential : potential;
  }
  [@@deriving eq, show]

  let head prim = { left = []; head = prim; right = []; potential = [] }

  let (!) dep  = head (Depname dep)
  let lanc dep = head (Anchor (Valency.Left, dep))
  let ranc dep = head (Anchor (Valency.Right, dep))

  let (>>) l r = { r with left = l.left @ [l.head] @ l.right @ r.left }
  let (<<) l r = { l with right = l.right @ r.left @ [r.head] @ r.right }
  let (^^) c p = { c with potential = c.potential @ p }

  let inner_left t  = List.last t.left
  let outer_left t  = List.first t.left
  let inner_right t = List.first t.right
  let outer_right t = List.last t.right

  let drop_inner_left  t = {t with left  = List.drop_last t.left}
  let drop_outer_left  t = {t with left  = try List.tl t.left with _ -> []}
  let drop_inner_right t = {t with right = try List.tl t.right with _ -> []}
  let drop_outer_right t = {t with right = List.drop_last t.right}

  let feed_left prim t =
    let (let*) = Option.bind in
    let* prim_left = inner_left t in

    if equal_primitive prim prim_left
    then Some (drop_inner_left t)
    else None

  let feed_right t prim =
    let (let*) = Option.bind in
    let* prim_right = inner_right t in

    if equal_primitive prim_right prim
    then Some (drop_inner_right t)
    else None

  let is_left_of a b = Dependency.compare_node a b = 1
  let is_right_of a b = Dependency.compare_node a b = -1

  let dir a b =
    if is_left_of a b
    then Valency.Left
    else Valency.Right

  let head_dir node =
    match Dependency.head node with
    | Some h -> dir h node
    | None -> failwith "(T_T) Sorry this is the root, there is no direction."

  let return_type node =
    match Dependency.deprel node with
    | Some dep ->
      if Dependency.is_dep_projective node
      then depname dep
      else anchor (head_dir node) dep
    | None -> root

  let head_is head node = match Dependency.head node with
    | None -> false
    | Some h -> Dependency.equal_node head h

  let would_be_projective head sub =
    Seq.append (Dependency.projection head) (Dependency.projection sub)
    |> Dependency.is_continuous

  let first_projective_governor node =
    Dependency.governors node
    |> Seq.skip
    |> Seq.find_first (fun g -> would_be_projective g node)

  let to_dependency_of head node =
    let (let*) = Option.bind in
    let opt_equal = Option.equal Dependency.equal_node in
    if head_is head node && Dependency.is_dep_projective node then
      let* dep = Dependency.deprel node in
      Some (depname dep)
    else if opt_equal (first_projective_governor node) (Some head) then
      let* dep = Dependency.deprel node in
      Some (anchor (dir head node) dep)
    else
      None

  let left_dependencies node =
    Dependency.(nodes (sentence node))
    |> Seq.filter (is_left_of node)
    |> Seq.filter_map (to_dependency_of node)
    |> List.of_seq

  let right_dependencies node =
    Dependency.(nodes (sentence node))
    |> Seq.filter (is_right_of node)
    |> Seq.filter_map (to_dependency_of node)
    |> List.of_seq

  let get_deprel node = Dependency.deprel node |> Option.get

  let left_potential node =
    Dependency.left_children node
    |> Seq.filter (fun n -> not (Dependency.is_dep_projective n))
    |> Seq.map (fun n -> Valency.left_pos (get_deprel n))
    |> List.of_seq

  let head_potential node =
    if Dependency.is_root node || Dependency.is_dep_projective node
    then []
    else [Valency.(make (head_dir node) Negative (get_deprel node))]

  let right_potential node =
    Dependency.right_children node
    |> Seq.filter (fun n -> not (Dependency.is_dep_projective n))
    |> Seq.map (fun n -> Valency.right_pos (get_deprel n))
    |> List.of_seq

  let potential node =
    left_potential node @ head_potential node @ right_potential node

  let vicinity node = {
    left = left_dependencies node;
    head = return_type node;
    right = right_dependencies node;
    potential = potential node;
  }

  let to_string t =
    "["
    ^ (List.map primitive_to_string t.left |> String.concat "\\")
    ^ (if List.is_empty t.left then "" else "\\")
    ^ primitive_to_string t.head
    ^ (if List.is_empty t.right then "" else "/")
    ^ (List.map primitive_to_string t.right |> String.concat "/")
    ^ "]"
    ^ potential_to_string t.potential
end
