module Dict = Map.Make(Text)

let (=) = Int.equal
let (<) a b = (Int.compare a b) = -1
let (>) a b = (Int.compare a b) =  1
let (<=) a b =
  let c = Int.compare a b in
  c = -1 || c = 0
let (>=) a b =
  let c = Int.compare a b in
  c = 0 || c = 1

module Seq = struct
  include Stdlib.Seq

  let rec exists pred seq = match seq () with
    | Seq.Nil -> false
    | Seq.Cons (x, next) ->
      if pred x
      then true
      else exists pred next

  let rec find_first pred seq = match seq () with
    | Seq.Nil -> None
    | Seq.Cons (x, next) ->
      if pred x
      then Some x
      else find_first pred next

  let rec for_all pred seq = match seq () with
    | Seq.Nil -> true
    | Seq.Cons (x, next) ->
      if pred x
      then for_all pred next
      else false

  let rec range ?(step = 1) a b () =
    if a = b
    then Seq.Cons (a, empty)
    else Seq.Cons (a, range (a+step) b)

  let rec scanl f prev seq () = match seq () with
    | Nil -> Cons (prev, empty)
    | Cons(h, t) -> Cons (prev, scanl f (f prev h) t)

  let skip seq =
    match seq () with
    | Seq.Nil -> Seq.empty
    | Seq.Cons (_, next) -> next
end

module Array = struct
  include Stdlib.Array

  let get_opt arr i =
    try Some (Array.get arr i)
    with Invalid_argument _ -> None

  let scanl f prev arr =
    arr |> Array.to_seq |> Seq.scanl f prev |> Array.of_seq

  let to_seq_rev arr =
    Seq.range ~step:(-1) (length arr - 1) 0
    |> Seq.map (fun i -> arr.(i))
end

module List = struct
  include Stdlib.List

  let rec drop_last l = match l with
    | [] -> []
    | _ :: [] -> []
    | h :: t -> h :: drop_last t

  let enumerate l =
    let rec enumerate' index l () =
      match l with
      | []  -> Seq.Nil
      | h::t -> Seq.Cons ((index, h), enumerate' (index + 1) t)
    in
    enumerate' 0 l

  let first l = match l with
    | [] -> None
    | h :: _ -> Some h

  let rec intersection eq xs ys =
    match xs with
    | [] -> []
    | h :: t -> if List.find_opt (eq h) ys |> Option.is_none
      then intersection eq t ys
      else h :: (intersection eq t ys)

  let is_empty l = match l with
    | [] -> true
    | _  -> false

  let last l =
    if is_empty l
    then None
    else List.nth_opt l (List.length l - 1)

  let max compare base l =
    let choose a b = if compare a b > 0 then a else b in
    List.fold_left choose base l

  let max_opt compare l =
    let choose a b = if compare (Option.get a) b > 0 then a else Some b in
    List.fold_left choose None l

  let rec scanl f prev l = match l with
    | [] -> [prev]
    | h::t -> prev :: (scanl f (f prev h) t)

  let union eq l1 l2 = l1 @ List.filter (fun x -> not (List.exists (eq x) l1)) l2

  let rec uniq equal l =
    let without x l = l |> List.filter (fun x' -> not (equal x x')) in
    match l with
    | [] -> []
    | h :: t -> h :: (uniq equal (t |> without h))
end
